# Covid 19 Statistics

Covid 19 Statistics backend project collecting and providing covid 19 statistics about countries 

## prerequisites
you will need docker installed on your computer

## Run project

run docker-compose up command on project root directory

```bash
docker componse-up
```
run prisma migration command to synchronize database to server

```bash
npx prisma migrate dev --name init
```

##  Populate countries table

run npx nestjs-command

```bash
npx nestjs-command batch-create:country
```

##  Run tests

run jest on terminal

```bash
jest
```

## Documentation

documentation:https://documenter.getpostman.com/view/1515259/UyrHeCpX

postman:https://www.postman.com/winter-meadow-574469/workspace/covid-19-statistics/overview

## License
[MIT](https://choosealicense.com/licenses/mit/)
