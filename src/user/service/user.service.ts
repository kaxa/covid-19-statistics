import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { User } from '@prisma/client';
import { CreateUserInput } from '../types/create-user.type';

@Injectable()
export class UserService {

  constructor(private prisma: PrismaService) {
  }

  /**
   * Creates new user
   * @param  {CreateUserInput} createUserInput  create user input {email,name,passwordHash}
   * @return {User} user object
   */
  async createUser(createUserInput: CreateUserInput) {

    return await this.prisma.user
      .create({
        data: {
          email: createUserInput.email,
          name: createUserInput.name,
          password: createUserInput.passwordHash,
        },
      });
  }

  /**
   * Gets User by id
   * @param  {string} email  user unique email
   * @return {User} user object
   */
  getUserByEmail(email: string): Promise<User> {
    return this.prisma.user.findUnique({
      where: {
        email: email,
      },
    });
  }

  /**
   * Gets User by id
   * @param  {number} userId  user id
   * @return {User} user object
   */
  getUserById(userId: number): Promise<User> {
    return this.prisma.user.findUnique({
      where: {
        id: userId,
      },
    });
  }

}
