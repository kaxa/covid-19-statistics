import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { PrismaService } from '../../prisma.service';

describe('UserService', () => {

  let service: UserService;
  let prisma: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService, PrismaService],
    }).compile();

    service = module.get<UserService>(UserService);
    prisma = module.get<PrismaService>(PrismaService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createUser', () => {

    it('should return created prisma user object', async () => {

      const mockPrismaResponse = {
        id: 1,
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        password: 'testPasswordHash',
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      prisma.user.create = jest.fn().mockReturnValueOnce(mockPrismaResponse);

      const result = await service.createUser({
        email: 'kaxax95@gmail.com',
        name: 'kaxa',
        passwordHash: 'testPasswordHash',
      });

      expect(result).toMatchObject(mockPrismaResponse);

    });

    it('should throw error when ', async () => {

      prisma.user.create = jest.fn().mockRejectedValue(new Error('error'));

      try {
        await service.createUser({
          email: 'kaxax95@gmail.com',
          name: 'kaxa',
          passwordHash: 'testPasswordHash',
        });
      } catch (e) {
        expect(e.message).toBe('error');
      }

    });

  });

  describe('getUserByEmail', () => {

    it('should return prisma user object', async () => {

      const mockPrismaResponse = {
        id: 1,
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        password: 'testPasswordHash',
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      prisma.user.findUnique = jest.fn().mockReturnValueOnce(mockPrismaResponse);

      const result = await service.getUserByEmail('kaxax95@gmail.com');

      expect(result).toMatchObject(mockPrismaResponse);

    });

  });

  describe('getUserById', () => {

    it('should return prisma user object', async () => {

      const mockPrismaResponse = {
        id: 1,
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        password: 'testPasswordHash',
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      prisma.user.findUnique = jest.fn().mockReturnValueOnce(mockPrismaResponse);

      const result = await service.getUserById(1);

      expect(result).toMatchObject(mockPrismaResponse);

    });

  });

});
