export type CreateUserInput = {
    name: string,
    email: string,
    passwordHash: string
}
