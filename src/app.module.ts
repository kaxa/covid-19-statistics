import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { CountryModule } from './country/country.module';
import { StatisticModule } from './statistic/statistic.module';
import { APP_FILTER } from '@nestjs/core';
import { BaseExceptionsFilter } from './core/exception-filter/base-exception-filter/base-exception-filter';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), AuthModule, UserModule, CountryModule, StatisticModule,
  ],
  controllers: [AppController],
  providers: [AppService,
    {
      provide: APP_FILTER,
      useClass: BaseExceptionsFilter,
    },
  ],
})
export class AppModule {
}
