import {Controller, Get, HttpCode, HttpStatus, Query, UseGuards} from '@nestjs/common';
import { StatisticService } from '../service/statistic.service';
import { Statistic } from '@prisma/client';
import {JwtAuthGuard} from "../../auth/guard/jwt-auth.guard";

@Controller('statistic')
export class StatisticController {

  constructor(private statisticService: StatisticService) {
  }

  @Get('')
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.OK)
  listStatistic(
    @Query('countryId') countryId,
    @Query('recordDate') recordDate,
  ): Promise<Array<Statistic>> {

    return this.statisticService.filterStatistics({
      countryId: +countryId, recordDate: recordDate,
    });

  }
}
