import { Test, TestingModule } from '@nestjs/testing';
import { StatisticController } from './statistic.controller';
import { ScheduleModule } from '@nestjs/schedule';
import { BullModule } from '@nestjs/bull';
import { HttpModule } from '@nestjs/axios';
import { FetchCountryStatisticsService } from '../cron-job/fetch-country-statistics/fetch-country-statistics';
import { StatisticService } from '../service/statistic.service';
import { PrismaService } from '../../prisma.service';
import { CountryService } from '../../country/service/country.service';
import { CountryStatisticsConsumer } from '../consumer/country-statistics/country-statistics.consumer';
import { ConfigService } from '@nestjs/config';

describe('StatisticController', () => {
  let controller: StatisticController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        BullModule.forRoot({
          redis: {
            host: process.env.REDIS_URL,
            port: +process.env.REDIS_PORT,
          },
        }),
        BullModule.registerQueue({
          name: process.env.BULL_QUEUE_NAME,
        }),
        HttpModule,
      ],
      controllers: [StatisticController],
      providers: [FetchCountryStatisticsService, StatisticService, PrismaService, CountryService, CountryStatisticsConsumer, ConfigService],
    }).compile();

    controller = module.get<StatisticController>(StatisticController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
