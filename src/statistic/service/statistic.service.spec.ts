import {Test, TestingModule} from '@nestjs/testing';
import {StatisticService} from './statistic.service';
import {FetchCountryStatisticsService} from '../cron-job/fetch-country-statistics/fetch-country-statistics';
import {PrismaService} from '../../prisma.service';
import {CountryService} from '../../country/service/country.service';
import {CountryStatisticsConsumer} from '../consumer/country-statistics/country-statistics.consumer';
import {ConfigService} from '@nestjs/config';
import {ScheduleModule} from '@nestjs/schedule';
import {BullModule} from '@nestjs/bull';
import {HttpModule} from '@nestjs/axios';
import {GetCountryStatisticsResponse} from '../types/get-country-statistics.type';
import {Prisma} from '@prisma/client';
import * as moment from "moment";

describe('StatisticService', () => {

    let service: StatisticService;
    let prisma: PrismaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ScheduleModule.forRoot(),
                BullModule.forRoot({
                    redis: {
                        host: process.env.REDIS_URL,
                        port: +process.env.REDIS_PORT,
                    },
                }),
                BullModule.registerQueue({
                    name: process.env.BULL_QUEUE_NAME,
                }),
                HttpModule,
            ],
            providers: [FetchCountryStatisticsService, StatisticService, PrismaService, CountryService, CountryStatisticsConsumer, ConfigService],
        }).compile();

        service = module.get<StatisticService>(StatisticService);
        prisma = module.get<PrismaService>(PrismaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('addOrUpdateStatisticRecord', () => {

        it('should call prisma.statistic.upsert with specific params', async () => {

            const countryId = 1;

            const getCountryStatisticsResponse: GetCountryStatisticsResponse = {
                id: 29,
                country: 'Georgia',
                code: 'GE',
                confirmed: 2665,
                recovered: 649,
                critical: 3784,
                deaths: 450,
                created_at: '2022-02-13T18:17:01.000000Z',
                updated_at: '2022-05-01T00:00:03.000000Z',
            };

            const recordDate: string = moment(new Date('2022-04-30T20:00:00.000Z')).format('MM/DD/YYYY');

            prisma.statistic.upsert = jest.fn().mockReturnValueOnce({
                id: 109,
                confirmed: 2665,
                recovered: 649,
                critical: 3784,
                countryId: 1,
                recordDate: '2022-04-29T20:00:00.000Z',
                createdAt: '2022-04-30T11:00:03.440Z',
                updatedAt: '2022-04-30T15:00:03.542Z',
            });

            const upsertToBeCalledWith: Prisma.StatisticUpsertArgs = {
                where: {
                    countryId_recordDate_unique_key: {
                        countryId: countryId,
                        recordDate: recordDate,
                    },
                },
                update: {
                    confirmed: getCountryStatisticsResponse.confirmed,
                    death: getCountryStatisticsResponse.deaths,
                    recovered: getCountryStatisticsResponse.deaths,
                },
                create: {
                    countryId: countryId,
                    confirmed: getCountryStatisticsResponse.confirmed,
                    death: getCountryStatisticsResponse.deaths,
                    recovered: getCountryStatisticsResponse.deaths,
                    recordDate: recordDate,
                },
            };

            await service.addOrUpdateStatisticRecord(countryId, getCountryStatisticsResponse);

            expect(prisma.statistic.upsert).toHaveBeenCalledWith(upsertToBeCalledWith);

        });

    });

    describe('filterStatistics', () => {

        it('should call prisma.statistic.findMany with specific params', async () => {

            prisma.statistic.findMany = jest.fn().mockReturnValueOnce([
                {
                    id: 106,
                    confirmed: 4442,
                    recovered: 1353,
                    death: 1353,
                    countryId: 1,
                    recordDate: "05/02/2022",
                    createdAt: '2022-04-30T11:00:00.435Z',
                    updatedAt: '2022-04-30T15:00:00.453Z',
                },
            ]);

            const findManyToBeCalledWithWhere = {
                countryId: 1,
                recordDate: "05/02/2022",
            };

            const findManyToBeCalledWith = {
                where: findManyToBeCalledWithWhere,
                include:{
                    country:true
                }
            };

            await service.filterStatistics({
                countryId: 1,
                recordDate: '2022/05/02',
            });

            expect(prisma.statistic.findMany).toHaveBeenCalledWith(findManyToBeCalledWith);

        });

        it('should return prisma.statistic.findMany mocked value ', async () => {

            const findManyResponse = [
                {
                    id: 106,
                    confirmed: 4442,
                    recovered: 1353,
                    death: 1353,
                    countryId: 1,
                    recordDate: '2022-04-29T20:00:00.000Z',
                    createdAt: '2022-04-30T11:00:00.435Z',
                    updatedAt: '2022-04-30T15:00:00.453Z',
                },
            ];
            prisma.statistic.findMany = jest.fn().mockReturnValueOnce(findManyResponse);

            const result = await service.filterStatistics({
                countryId: 1,
                recordDate: '2022-04-29T21:00:00.000Z',
            });

            expect(result).toMatchObject(findManyResponse);

        });

    });

});
