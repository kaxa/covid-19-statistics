import {Injectable} from '@nestjs/common';
import {PrismaService} from '../../prisma.service';
import {Statistic, Prisma} from '@prisma/client';
import {GetCountryStatisticsResponse} from '../types/get-country-statistics.type';
import {FilterStatisticQueryInput} from '../types/filter-statistic.type';
import {buildStatisticQuery} from '../helper/helper';
import * as moment from "moment";

@Injectable()
export class StatisticService {

    constructor(private prisma: PrismaService) {
    }

    /**
     * Filters and return country statistics
     * @param  {FilterStatisticQueryInput} filterStatisticQueryInput
     * @return  {Array<Statistic>} statistics array
     */
    async filterStatistics(filterStatisticQueryInput: FilterStatisticQueryInput): Promise<Array<Statistic>> {

        const statisticWhereInput: Prisma.StatisticWhereInput = buildStatisticQuery(filterStatisticQueryInput);

        return this.prisma.statistic.findMany({
            where: statisticWhereInput,
            include:{
                country:true
            }
        });

    }

    /**
     * add new controller record
     * checks if controller record exists with specific day and countryId
     * updates existing record if day records exists otherwise creates new record for day
     * @param  {number} countryId  - country id for controller record
     * @param  {GetCountryStatisticsResponse} input  - country controller data
     */
    async addOrUpdateStatisticRecord(countryId: number, input: GetCountryStatisticsResponse) {

        const recordDateUnFormatted = new Date(input.updated_at);

        const recordDate:string =
            moment(recordDateUnFormatted).format('MM/DD/YYYY');

        await this.prisma.statistic.upsert({
            where: {
                countryId_recordDate_unique_key: {
                    countryId: countryId,
                    recordDate: recordDate,
                },
            },
            update: {
                confirmed: input.confirmed,
                death: input.deaths,
                recovered: input.deaths,
            },
            create: {
                countryId: countryId,
                confirmed: input.confirmed,
                death: input.deaths,
                recovered: input.deaths,
                recordDate: recordDate,
            },
        });

    }

}
