export type FilterStatisticQueryInput = {
  countryId: number,
  recordDate: string
}
