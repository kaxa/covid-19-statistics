export type GetCountryStatisticsResponse = {
    id: number,
    country: string,
    code: string,
    confirmed: number,
    recovered: number,
    critical: number,
    deaths: number,
    created_at: string,
    updated_at: string
}
