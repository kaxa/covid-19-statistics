import { Test, TestingModule } from '@nestjs/testing';
import { StatisticService } from '../../service/statistic.service';
import { ScheduleModule } from '@nestjs/schedule';
import { BullModule } from '@nestjs/bull';
import { HttpModule, HttpService } from '@nestjs/axios';
import { StatisticController } from '../../controller/statistic.controller';
import { PrismaService } from '../../../prisma.service';
import { CountryService } from '../../../country/service/country.service';
import { ConfigService } from '@nestjs/config';
import { FetchCountryStatisticsService } from '../../cron-job/fetch-country-statistics/fetch-country-statistics';
import { CountryStatisticsConsumer } from './country-statistics.consumer';
import { of } from 'rxjs';

describe('CountryStatisticsConsumer', () => {

  let service: CountryStatisticsConsumer;
  let statisticService: StatisticService;
  let httpService: HttpService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        BullModule.forRoot({
          redis: {
            host: process.env.REDIS_URL,
            port: +process.env.REDIS_PORT,
          },
        }),
        BullModule.registerQueue({
          name: process.env.BULL_QUEUE_NAME,
        }),
        HttpModule,
      ],
      controllers: [StatisticController],
      providers: [FetchCountryStatisticsService, StatisticService, PrismaService, CountryService, CountryStatisticsConsumer, ConfigService],
    }).compile();

    service = app.get<CountryStatisticsConsumer>(CountryStatisticsConsumer);
    statisticService = app.get<StatisticService>(StatisticService);
    httpService = app.get<HttpService>(HttpService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('CountryStatisticsConsumer',  () => {

    it('it should call addOrUpdateStatisticRecord with params', async () => {

      statisticService.addOrUpdateStatisticRecord = jest.fn().mockReturnValueOnce({});

      const job: any = {
        data: {
          id: 29,
          code: 'KA',
          name: {
            en: 'ka',
            ka: 'ka',
          },
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      };
      const httpResponseData = {
        data: {
          'id': 29,
          'country': 'Georgia',
          'code': 'GE',
          'confirmed': 2665,
          'recovered': 649,
          'critical': 3784,
          'deaths': 450,
          'created_at': '2022-02-13T18:17:01.000000Z',
          'updated_at': '2022-05-01T00:00:03.000000Z',

        },
        headers: {},
        config: { url: 'http://localhost:3000/mockUrl' },
        status: 200,
        statusText: 'OK',
      };

      jest
        .spyOn(httpService, 'post')
        .mockImplementationOnce(() => of(httpResponseData));

      await service.transcode(job);

      expect(statisticService.addOrUpdateStatisticRecord).toHaveBeenCalledWith(job.data.id, httpResponseData.data);

    });

  });

});
