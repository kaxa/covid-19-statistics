import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';
import { Country } from '@prisma/client';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { Logger } from '@nestjs/common';
import { map } from 'rxjs';
import { GetCountryStatisticsResponse } from '../../types/get-country-statistics.type';
import { StatisticService } from '../../service/statistic.service';

@Processor('country-statistics')
export class CountryStatisticsConsumer {

  private readonly logger = new Logger(CountryStatisticsConsumer.name);

  constructor(private httpService: HttpService,
              private config: ConfigService,
              private statisticService: StatisticService,
  ) {
  }

  /**
   * consumer for updating statistics data
   * get country controller data from external service
   * @param  {Job<any>} job  -data for queue job
   */
  @Process()
  async transcode(job: Job<unknown>) {

    const country: Country = job.data as Country;

    this.httpService.post(this.config.get<string>('GET_STATISTICS_DATA_URL'), {
      code: country.code,
    }).pipe(map(response => response.data)).subscribe(
      {
        next: async (data: GetCountryStatisticsResponse) => {
          await this.statisticService.addOrUpdateStatisticRecord(country.id, data);
        },
        error: (error) => {
          this.logger.error(error);
        },
      },
    );
  }

}
