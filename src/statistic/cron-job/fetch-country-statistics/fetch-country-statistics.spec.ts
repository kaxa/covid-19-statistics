import { Test, TestingModule } from '@nestjs/testing';
import { StatisticService } from '../../service/statistic.service';
import { FetchCountryStatisticsService } from './fetch-country-statistics';
import { ScheduleModule } from '@nestjs/schedule';
import { BullModule } from '@nestjs/bull';
import { HttpModule } from '@nestjs/axios';
import { StatisticController } from '../../controller/statistic.controller';
import { PrismaService } from '../../../prisma.service';
import { CountryService } from '../../../country/service/country.service';
import { CountryStatisticsConsumer } from '../../consumer/country-statistics/country-statistics.consumer';
import { ConfigService } from '@nestjs/config';
import { ListCountriesWithStatistic } from '../../../country/types/list-countries.types';
import {sleep} from "../../../core/helper/helper";

describe('FetchCountryStatisticsService', () => {

  let service: FetchCountryStatisticsService;
  let countryService: CountryService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ScheduleModule.forRoot(),
        BullModule.forRoot({
          redis: {
            host: process.env.REDIS_URL,
            port: +process.env.REDIS_PORT,
          },
        }),
        BullModule.registerQueue({
          name: process.env.BULL_QUEUE_NAME,
        }),
        HttpModule,
      ],
      controllers: [StatisticController],
      providers: [FetchCountryStatisticsService, StatisticService, PrismaService, CountryService, CountryStatisticsConsumer, ConfigService],
    }).compile();

    countryService = app.get<CountryService>(CountryService);
    service = app.get<FetchCountryStatisticsService>(FetchCountryStatisticsService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('FetchCountryStatisticsService', () => {

    it('should call queue add on specific times', async () => {

      const listCountriesMockValue: Array<ListCountriesWithStatistic> = [
        {
          id: 1,
          code: 'GE',
          name: {
            en: 'georgia',
            ka: 'საქართველო',
          },
          updatedAt: new Date('2022-02-13T18:17:01.000000Z'),
          createdAt: new Date('2022-02-13T18:17:01.000000Z'),
          lastStatisticRecord: null,
        },
        {
          id: 1,
          code: 'GE',
          name: {
            en: 'georgia',
            ka: 'საქართველო',
          },
          updatedAt: new Date('2022-02-13T18:17:01.000000Z'),
          createdAt: new Date('2022-02-13T18:17:01.000000Z'),
          lastStatisticRecord: null,
        },
      ];

      countryService.listCountries = jest.fn().mockReturnValueOnce(listCountriesMockValue);

      service['countryStatisticsQueue'].add = jest.fn().mockReturnValueOnce({});

      await service.fetchCountryStatistics();

      expect(service['countryStatisticsQueue'].add).toBeCalledTimes(2);

    });

    it('should call queue add with time intervals', async () => {

      const listCountriesMockValue: Array<ListCountriesWithStatistic> = [
        {
          id: 1,
          code: 'GE',
          name: {
            en: 'georgia',
            ka: 'საქართველო',
          },
          updatedAt: new Date('2022-02-13T18:17:01.000000Z'),
          createdAt: new Date('2022-02-13T18:17:01.000000Z'),
          lastStatisticRecord: null,
        },
        {
          id: 1,
          code: 'GE',
          name: {
            en: 'georgia',
            ka: 'საქართველო',
          },
          updatedAt: new Date('2022-02-13T18:17:01.000000Z'),
          createdAt: new Date('2022-02-13T18:17:01.000000Z'),
          lastStatisticRecord: null,
        },
      ];

      countryService.listCountries = jest.fn().mockReturnValueOnce(listCountriesMockValue);

      service.fetchCountryStatistics().then();

      service['countryStatisticsQueue'].add = jest.fn().mockReturnValueOnce({});

      expect(service['countryStatisticsQueue'].add).toBeCalledTimes(0);

      await sleep(1000);

      expect(service['countryStatisticsQueue'].add).toBeCalledTimes(1);

      await sleep(1000);

      expect(service['countryStatisticsQueue'].add).toBeCalledTimes(2);

    });

  });

});
