import {Injectable, Logger} from '@nestjs/common';
import {Cron, CronExpression} from '@nestjs/schedule';
import {InjectQueue} from '@nestjs/bull';
import {Queue} from 'bull';
import {CountryService} from '../../../country/service/country.service';
import {sleep} from "../../../core/helper/helper";

@Injectable()
export class FetchCountryStatisticsService {

    private readonly logger = new Logger(FetchCountryStatisticsService.name);

    constructor(
        @InjectQueue('country-statistics') private countryStatisticsQueue: Queue,
        private countryService: CountryService,
    ) {}

    /**
     * cron job to fetch country statistics
     */
    @Cron(CronExpression.EVERY_HOUR)
    async fetchCountryStatistics() {
        await this.startFetchingCountryStatistics()
    }


    /**
     * gets all country from database
     * send every country record to queue
     * with 1 second interval to avoid server too many requests error
     */
    private async startFetchingCountryStatistics() {

        this.logger.debug('start country statistic polling');

        const countries = await this.countryService.listCountries();

        for (const country of countries) {
            await this.countryStatisticsQueue.add(country);
            await sleep(1000);
        }

    }

}

