import {FilterStatisticQueryInput} from '../types/filter-statistic.type';
import {Prisma} from '@prisma/client';
import * as moment from "moment";

/**
 * iterates over filterStatisticQueryInput params and build Prisma.StatisticWhereInput
 * @param  {FilterStatisticQueryInput} filterStatisticQueryInput
 * @return  Prisma.StatisticWhereInput
 */
export function buildStatisticQuery(filterStatisticQueryInput: FilterStatisticQueryInput) {

    const statisticWhereInput: Prisma.StatisticWhereInput = {};

    const queryKeys = Object.keys(filterStatisticQueryInput);

    for (const key of queryKeys) {
        const item = filterStatisticQueryInput[key];
        if (item !== null && item !== undefined && !Number.isNaN(item)) {
            statisticWhereInput[key] = item;
        }
    }

    if (statisticWhereInput.recordDate) {
        const recordDate = new Date(filterStatisticQueryInput.recordDate);
        statisticWhereInput.recordDate = moment(recordDate).format('MM/DD/YYYY');
    }

    return statisticWhereInput;

}
