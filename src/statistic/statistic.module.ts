import {Module} from '@nestjs/common';
import {ScheduleModule} from "@nestjs/schedule";
import {FetchCountryStatisticsService} from "./cron-job/fetch-country-statistics/fetch-country-statistics";
import {BullModule} from "@nestjs/bull";
import {StatisticService} from './service/statistic.service';
import {CountryService} from "../country/service/country.service";
import {PrismaService} from "../prisma.service";
import {CountryStatisticsConsumer} from "./consumer/country-statistics/country-statistics.consumer";
import {HttpModule} from "@nestjs/axios";
import {ConfigService} from "@nestjs/config";
import { StatisticController } from './controller/statistic.controller';

@Module({
    imports: [
        ScheduleModule.forRoot(),
        BullModule.forRoot({
            redis: {
                host: process.env.REDIS_URL,
                port: +process.env.REDIS_PORT,
            },
        }),
        BullModule.registerQueue({
            name: process.env.BULL_QUEUE_NAME,
        }),
        HttpModule,
    ],
    controllers: [StatisticController],
    providers: [FetchCountryStatisticsService, StatisticService, PrismaService, CountryService, CountryStatisticsConsumer, ConfigService],
})
export class StatisticModule {

}
