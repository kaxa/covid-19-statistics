import {IsNotEmpty, IsString} from "class-validator";

export class BatchCreateCountryDto {
    countries: Array<BatchCreateCountryDtoCountry>
}

export class BatchCreateCountryDtoCountry {

    @IsNotEmpty()
    @IsString()
    code: string;

    name: CountryName;

}

export type CountryName = {
    en: string
    ka: string
}

