import {Module} from '@nestjs/common';
import {CountryService} from './service/country.service';
import {CountryController} from './controller/country.controller';
import {PrismaService} from "../prisma.service";
import {CountryCommand} from "./command/country.command";
import {CommandModule} from "nestjs-command";
import {HttpModule} from "@nestjs/axios";
import {ConfigService} from "@nestjs/config";

@Module({
    imports: [CommandModule, HttpModule],
    providers: [CountryService, PrismaService, CountryCommand,ConfigService],
    controllers: [CountryController]
})
export class CountryModule {
}
