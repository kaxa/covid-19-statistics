import { CountryName } from '../dto/batch-create-country.dto';
import { Statistic } from '@prisma/client';

export type ListCountriesWithStatistic = {
  id: number
  code: string
  name: CountryName
  lastStatisticRecord:Statistic
  createdAt: Date
  updatedAt: Date
}

