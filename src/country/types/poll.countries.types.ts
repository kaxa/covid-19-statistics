export type PollCountriesCountry = {
    code: string,
    name: PollCountriesCountryName
}
export type PollCountriesCountryName = {
    en: string,
    ka: string
}
