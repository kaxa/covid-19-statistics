import {Controller, Get, HttpCode, HttpStatus, UseGuards} from '@nestjs/common';
import { CountryService } from '../service/country.service';
import { ListCountriesWithStatistic } from '../types/list-countries.types';
import {JwtAuthGuard} from "../../auth/guard/jwt-auth.guard";

@Controller('country')
export class CountryController {

  constructor(private countryService: CountryService) {
  }

  @Get('')
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.OK)
  listCountries(): Promise<Array<ListCountriesWithStatistic>> {
    return this.countryService.listCountries();
  }

}
