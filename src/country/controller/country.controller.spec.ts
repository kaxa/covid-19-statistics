import { Test, TestingModule } from '@nestjs/testing';
import { CountryController } from './country.controller';
import { CommandModule } from 'nestjs-command';
import { HttpModule } from '@nestjs/axios';
import { CountryService } from '../service/country.service';
import { PrismaService } from '../../prisma.service';
import { CountryCommand } from '../command/country.command';
import { ConfigService } from '@nestjs/config';
import { ListCountriesWithStatistic } from '../types/list-countries.types';
import * as moment from "moment";

describe('CountryController', () => {

  let controller: CountryController;
  let countryService: CountryService;

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      imports: [CommandModule, HttpModule],
      providers: [CountryService, PrismaService, CountryCommand, ConfigService],
      controllers: [CountryController],
    }).compile();

    controller = module.get<CountryController>(CountryController);
    countryService = module.get<CountryService>(CountryService);

  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('listCountries', () => {

    it('should return countryService.listCountries() mocked value', async () => {

      const listCountriesResult:Array<ListCountriesWithStatistic> = [
        {
          id: 1,
          code: 'AF',
          name: {
            en: 'Afghanistan',
            ka: 'ავღანეთი',
          },
          lastStatisticRecord: {
            id: 211,
            confirmed: 3735,
            recovered: 202,
            death: 202,
            countryId: 1,
            recordDate: moment(new Date('2022-04-30T20:00:00.000Z')).format('MM/DD/YYYY'),
            createdAt: new Date('2022-05-01T14:00:00.534Z'),
            updatedAt: new Date('2022-05-01T15:00:00.446Z'),
          },
          createdAt: new Date('2022-04-30T10:06:10.674Z'),
          updatedAt: new Date('2022-04-30T10:07:50.572Z'),
        }
      ];

      countryService.listCountries = jest.fn().mockReturnValueOnce(listCountriesResult);

      const result = await controller.listCountries();

      expect(result).toMatchObject(listCountriesResult);

    });

  });

});
