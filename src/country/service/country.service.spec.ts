import { Test, TestingModule } from '@nestjs/testing';
import { CountryService } from './country.service';
import { PrismaService } from '../../prisma.service';
import { CountryCommand } from '../command/country.command';
import { ConfigService } from '@nestjs/config';
import { CommandModule } from 'nestjs-command';
import { HttpModule } from '@nestjs/axios';
import { BatchCreateCountryDto } from '../dto/batch-create-country.dto';

describe('CountryService', () => {

  let service: CountryService;
  let prisma: PrismaService;

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      imports: [CommandModule, HttpModule],
      providers: [CountryService, PrismaService, CountryCommand, ConfigService],
    }).compile();

    service = module.get<CountryService>(CountryService);
    prisma = module.get<PrismaService>(PrismaService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('batchCreateCountries', () => {

    it('should return mocked prisma transaction object', async () => {

      const batchCreateCountryDto: BatchCreateCountryDto = {
        countries: [{
          code: 'KA',
          name: {
            en: 'Georgia',
            ka: 'საქართველო',
          },
        }],
      };

      const mockObjectResponse = [
        {
          id: 25,
          code: 'KA',
          name: { en: 'Georgia', ka: 'საქართველო' },
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ];

      prisma.$transaction = jest.fn().mockReturnValueOnce(mockObjectResponse);

      const result = await service.batchCreateCountries(batchCreateCountryDto);

      expect(result).toMatchObject(mockObjectResponse);

    });

  });

  describe('listCountries', () => {

    it('should return formatted prisma country statistic record', async () => {

      const mockObjectResponse = [
        {
          id: 1,
          code: 'AF',
          name: {
            en: 'Afghanistan',
            ka: 'ავღანეთი',
          },
          statistics: [
            {
              id: 211,
              confirmed: 3735,
              recovered: 202,
              death: 202,
              countryId: 1,
              recordDate: '2022-04-30T20:00:00.000Z',
              createdAt: '2022-05-01T14:00:00.534Z',
              updatedAt: '2022-05-01T15:00:00.446Z',
            },
          ],
          createdAt: '2022-04-30T10:06:10.674Z',
          updatedAt: '2022-04-30T10:07:50.572Z',
        },
      ];

      prisma.country.findMany = jest.fn().mockReturnValueOnce(mockObjectResponse);

      const expectedResult = [
        {
          id: 1,
          code: 'AF',
          name: {
            en: 'Afghanistan',
            ka: 'ავღანეთი',
          },
          lastStatisticRecord: {
            id: 211,
            confirmed: 3735,
            recovered: 202,
            death: 202,
            countryId: 1,
            recordDate: '2022-04-30T20:00:00.000Z',
            createdAt: '2022-05-01T14:00:00.534Z',
            updatedAt: '2022-05-01T15:00:00.446Z',
          },
          createdAt: '2022-04-30T10:06:10.674Z',
          updatedAt: '2022-04-30T10:07:50.572Z',
        },
      ];

      const result = await service.listCountries();

      expect(result).toMatchObject(expectedResult);

    });

    it('should call prisma.country.findMany with params', async () => {

      const mockObjectResponse = [
        {
          id: 1,
          code: 'AF',
          name: {
            en: 'Afghanistan',
            ka: 'ავღანეთი',
          },
          statistics: [
            {
              id: 211,
              confirmed: 3735,
              recovered: 202,
              death: 202,
              countryId: 1,
              recordDate: '2022-04-30T20:00:00.000Z',
              createdAt: '2022-05-01T14:00:00.534Z',
              updatedAt: '2022-05-01T15:00:00.446Z',
            },
          ],
          createdAt: '2022-04-30T10:06:10.674Z',
          updatedAt: '2022-04-30T10:07:50.572Z',
        },
      ];

      prisma.country.findMany = jest.fn().mockReturnValueOnce(mockObjectResponse);

      const tobeCalledWith = {
        include: {
          statistics: {
            orderBy: {
              recordDate: 'desc',
            },
            take: 1,
          },
        },
      };

      await service.listCountries();

      expect(prisma.country.findMany).toHaveBeenCalledWith(tobeCalledWith);

    });

  });

});
