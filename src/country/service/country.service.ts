import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma.service';
import { BatchCreateCountryDto, CountryName } from '../dto/batch-create-country.dto';
import { ListCountriesWithStatistic } from '../types/list-countries.types';

@Injectable()
export class CountryService {

  constructor(private prisma: PrismaService) {
  }

  /**
   * batch create of countries
   * @param  {BatchCreateCountryDto} batchCreateCountryDto list of countryDto object
   */
  async batchCreateCountries(batchCreateCountryDto: BatchCreateCountryDto) {

    return await this.prisma.$transaction(

      batchCreateCountryDto.countries.map(item =>

        this.prisma.country.upsert({
          where: { code: item.code },
          update: {
            name: item.name,
          },
          create: {
            name: item.name,
            code: item.code,
          },
        }),
      ),
    );

  }

  /**
   * List countries with last record of controller
   * @return {[ListCountriesWithStatistic]} array of countries with controller
   */
  async listCountries(): Promise<Array<ListCountriesWithStatistic>> {
    const countries = await this.prisma.country.findMany({
      include: {
        statistics: {
          orderBy: {
            recordDate: 'desc',
          },
          take: 1,
        },
      },
    });

    return countries.map(x => {
      return {
        id: x.id,
        code: x.code,
        name: x.name as CountryName,
        lastStatisticRecord: x.statistics[0],
        createdAt: x.createdAt,
        updatedAt: x.updatedAt,
      };
    });

  }

}
