import {Command} from 'nestjs-command';
import {Injectable} from '@nestjs/common';
import {CountryService} from '../service/country.service';
import {HttpService} from '@nestjs/axios';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class CountryCommand {

    constructor(
        private readonly countryService: CountryService,
        private httpService: HttpService,
        private config: ConfigService
    ) {
    }

    /**
     * Command for cli
     * get countries data from external service and saves data to db
     */
    @Command({command: 'batch-create:country', describe: 'batch create countries'})
    async batchCreateCountries() {

        await this.httpService.get(this.config.get<string>('GET_COUNTRIES_DATA_URL')).subscribe(async response => {

                await this.countryService.batchCreateCountries({
                    countries: response.data,
                });

                console.log('successfully synchronized countries');

            },
        );

    }

}
