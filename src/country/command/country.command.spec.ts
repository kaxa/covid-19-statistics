import {Test, TestingModule} from '@nestjs/testing';
import {CountryCommand} from './country.command';
import {CommandModule} from 'nestjs-command';
import {HttpModule, HttpService} from '@nestjs/axios';
import {CountryService} from '../service/country.service';
import {PrismaService} from '../../prisma.service';
import {ConfigService} from '@nestjs/config';
import {CountryController} from '../controller/country.controller';
import {of} from 'rxjs';

describe('CountryCommand', () => {

    let countryCommand: CountryCommand;
    let countryService: CountryService;
    let httpService: HttpService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [CommandModule, HttpModule],
            providers: [CountryService, PrismaService, CountryCommand, ConfigService],
            controllers: [CountryController],
        }).compile();

        countryCommand = module.get<CountryCommand>(CountryCommand);
        countryService = module.get<CountryService>(CountryService);
        httpService = module.get<HttpService>(HttpService);

    });

    it('should be defined', () => {
        expect(countryCommand).toBeDefined();
    });

    describe('batchCreateCountries', () => {

        it('should get data http request and call countryService.batchCreateCountries', async () => {

            const httpResponseData = {
                data: [
                    {
                        'code': 'AF',
                        'name': {
                            'en': 'Afghanistan',
                            'ka': 'ავღანეთი',
                        },
                    }],
                headers: {},
                config: {url: 'http://localhost:3000/mockUrl'},
                status: 200,
                statusText: 'OK',
            };

            jest
                .spyOn(httpService, 'get')
                .mockImplementationOnce(() => of(httpResponseData));

            countryService.batchCreateCountries = jest.fn().mockResolvedValueOnce({});

            await countryCommand.batchCreateCountries();

            expect(countryService.batchCreateCountries).toHaveBeenCalledWith({
                countries: httpResponseData.data,
            });

        });

    });

});
