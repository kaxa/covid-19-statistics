import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from '../service/auth.service';
import { UserService } from '../../user/service/user.service';
import { PrismaService } from '../../prisma.service';
import { JwtStrategy } from '../jwt-strategy';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { SignInDto } from '../dto/auth.dto';

describe('AuthController', () => {

  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({}),
      ],
      providers: [AuthService, UserService, PrismaService, JwtStrategy, ConfigService],

    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);

  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {

    it('should return authService.signUp mocked value', async () => {

      const signUpServiceResult = {
        id: 1,
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        updatedAt: new Date(),
        createdAt: new Date(),
      };

      authService.signUp = jest.fn().mockReturnValueOnce(signUpServiceResult);

      const signUpDto = {
        email: "kaxax95@gmail.com",
        name: "kaxax95@gmail.com",
        password: '12345',
      };

      const result = await controller.register(signUpDto);

      expect(result).toMatchObject(signUpServiceResult);

    });

  });

  describe('login', () => {

    it('should return authService.signIn mocked value', async () => {

      const signInServiceResult = {
        accessToken:"testAccessToken"
      };

      authService.signIn = jest.fn().mockReturnValueOnce(signInServiceResult);

      const signInDto:SignInDto = {
        email: 'null',
        password: '12345',
      };

      const result = await controller.login(signInDto);

      expect(result).toMatchObject(signInServiceResult);

    });

  });

});
