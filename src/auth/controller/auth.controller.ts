import {Body, Controller, HttpCode, HttpStatus, Post} from '@nestjs/common';
import {AuthService} from "../service/auth.service";
import {SignInDto, SignUpDto} from "../dto/auth.dto";
import { SignInResponse, SignUpResponse } from '../types/auth.types';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {
    }

    @Post('register')
    @HttpCode(HttpStatus.CREATED)
    register(@Body() signUpDto: SignUpDto): Promise<SignUpResponse> {
        return this.authService.signUp(signUpDto);
    }

    @Post('login')
    @HttpCode(HttpStatus.OK)
    login(@Body() signInDto: SignInDto): Promise<SignInResponse> {
        return this.authService.signIn(signInDto);
    }
}
