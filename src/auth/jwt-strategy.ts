import {Injectable, UnauthorizedException} from "@nestjs/common";
import {PassportStrategy} from "@nestjs/passport";
import {Strategy, ExtractJwt} from 'passport-jwt'
import {User} from "@prisma/client";
import {UserService} from "../user/service/user.service";
import {JwtPayload} from "./types/jwt-payload.type";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {

    constructor(private userService: UserService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.ACCESS_TOKEN_SECRET
        })
    }

    /**
     * Validates if user exists
     * @param  {JwtPayload} payload  user unique email
     * @return {User} user object
     */
    async validate(payload: JwtPayload): Promise<User> {
        const {sub} = payload
        const user = await this.userService.getUserById(sub)
        if (!user) {
            throw new UnauthorizedException()
        }
        return user
    }
}
