import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UserService } from '../../user/service/user.service';
import { PrismaService } from '../../prisma.service';
import { JwtStrategy } from '../jwt-strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { SignInResponse, SignUpResponse } from '../types/auth.types';
import { SignInDto } from '../dto/auth.dto';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../types/jwt-payload.type';

describe('AuthService', () => {

  let service: AuthService;
  let jwtService: JwtService;
  let userService: UserService;

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({}),
      ],
      providers: [AuthService, UserService, PrismaService, JwtStrategy, ConfigService],
    }).compile();

    service = module.get<AuthService>(AuthService);
    jwtService = module.get<JwtService>(JwtService);
    userService = module.get<UserService>(UserService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('sign up', () => {

    it('should return user after successful signup', async () => {

      const createUserServiceResponseMock = {
        id: 1,
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        updatedAt: new Date(),
        createdAt: new Date(),
      }

      userService.createUser = jest.fn().mockReturnValueOnce(createUserServiceResponseMock);

      const toCreateUser = {
        name: 'kaxa',
        email: 'kaxax95@gmail.com',
        password: 'testPassword',
      };

      const signUpResponse: SignUpResponse = await service.signUp(toCreateUser);

      expect(signUpResponse).toMatchObject(createUserServiceResponseMock);

    });

  });

  describe('login', () => {

    it('should return access token', async () => {

      const signInDto: SignInDto = {
        email: 'kaxax95@gmail.com',
        password: '123456',
      };

      service['validateUser'] = jest.fn().mockReturnValueOnce({
        email: 'kaxax95@gmail.com',
        sub: 1,
      });

      const mockAccessToken = 'MOCK_ACCESS_TOKEN;';
      jwtService.signAsync = jest.fn().mockReturnValueOnce(mockAccessToken);

      const signInResponse: SignInResponse = await service.signIn(signInDto);

      expect(signInResponse.accessToken).toBe(mockAccessToken);

    });

    it('should error (Invalid credentials)) when user dont exists', async () => {

      const signInDto: SignInDto = {
        email: 'test@gmail.com1',
        password: '123456',
      };

      service['validateUser'] = jest.fn().mockRejectedValue(new Error('Invalid credentials'));

      try {
        await service.signIn(signInDto);
      } catch (e) {
        expect(e.message).toBe('Invalid credentials');
      }

    });

    it('should error on login (Invalid credentials)) when validate function can not validate credentials', async () => {

      const signInDto: SignInDto = {
        email: 'kaxax95@gmail.com1',
        password: '123456',
      };

      service['validateUser'] = jest.fn().mockReturnValueOnce(null);

      try {
        await service.signIn(signInDto);
      } catch (e) {
        expect(e.message).toBe('Invalid credentials');
      }

    });

  });

  describe('validate', () => {

    it('should throw (Invalid credentials))', async () => {

      const signInDto: SignInDto = {
        email: 'kaxax95@gmail.com1',
        password: '123456',
      };

      userService.getUserByEmail = jest.fn().mockReturnValueOnce(null);

      try {
        await service['validateUser'](signInDto);
      } catch (e) {
        expect(e.message).toBe('Invalid credentials');
      }

    });

    it('should return null', async () => {

      const signInDto: SignInDto = {
        email: 'kaxax95@gmail.com',
        password: '123456',
      };

      const passwordHash = await bcrypt.hash(signInDto.password, 10);

      userService.getUserByEmail = jest.fn().mockReturnValueOnce({
        password: passwordHash,
      });

      jest.spyOn(bcrypt, 'compare').mockImplementation(() => false);

      const validateResult = await service['validateUser'](signInDto);

      expect(validateResult).toBe(null);

    });

    it('should return JwtPayload object', async () => {

      const signInDto: SignInDto = {
        email: 'kaxax95@gmail.com',
        password: '123456',
      };

      userService.getUserByEmail = jest.fn().mockReturnValueOnce({
        id: 1,
        email: 'kaxax95@gmail.com',
      });

      jest.spyOn(bcrypt, 'compare').mockImplementation(() => true);

      const validateResult: JwtPayload = await service['validateUser'](signInDto);

      expect(validateResult).toMatchObject({
        sub: 1,
        email: 'kaxax95@gmail.com',
      });

    });

  });


});
