import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../../user/service/user.service';
import { JwtService } from '@nestjs/jwt';
import { SignInDto, SignUpDto } from '../dto/auth.dto';
import { ConfigService } from '@nestjs/config';
import { SignInResponse, SignUpResponse } from '../types/auth.types';
import { JwtPayload } from '../types/jwt-payload.type';
import * as bcrypt from 'bcrypt';

/**
 * Auth service
 */
@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private config: ConfigService,
  ) {
  }

  /**
   * Handles user sing in request
   * @param  {SignUpDto} signUpDto  sign in dto
   * @return {SignInResponse} sign in response for user
   */
  async signUp(signUpDto: SignUpDto): Promise<SignUpResponse> {

    const passwordHash = await bcrypt.hash(signUpDto.password, +this.config.get<string>('SALT_OR_ROUNDS'));

    const newUser = await this.userService.createUser({
      email: signUpDto.email,
      passwordHash: passwordHash,
      name: signUpDto.name,
    });

    return {
      id: newUser.id,
      name: newUser.name,
      email: newUser.email,
      updatedAt: newUser.updatedAt,
      createdAt: newUser.createdAt,
    };

  }

  /**
   * handles user sing in request
   * @param  {SignInDto} signInDto  sign in dto
   * @return {SignInResponse} sign in response for user
   */
  async signIn(signInDto: SignInDto): Promise<SignInResponse> {

    const validateResponse: JwtPayload = await this.validateUser(signInDto);

    if (!validateResponse) {
      throw new BadRequestException('Invalid credentials');
    }

    const accessToken = await this.jwtService.signAsync(validateResponse, {
      secret: this.config.get<string>('ACCESS_TOKEN_SECRET'),
      expiresIn: this.config.get<string>('ACCESS_TOKEN_EXPIRES'),
    });

    return {
      accessToken,
    };
  }

  /**
   * Checks user in database by email and password hash
   * @param  {SignInDto} signInDto user sing in dto
   * @return {JwtPayload} payload for jwt access token
   */
  private async validateUser(signInDto: SignInDto): Promise<JwtPayload> {

    const user = await this.userService.getUserByEmail(signInDto.email);

    if (!user) {
      throw new BadRequestException('Invalid credentials');
    }

    const isMatch = await bcrypt.compare(signInDto.password, user.password);

    if (user && isMatch) {
      return {
        email: user.email,
        sub: user.id,
      };
    }
    return null;
  }

}
