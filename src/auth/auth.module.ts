import {Module} from '@nestjs/common';
import {AuthService} from './service/auth.service';
import {UserService} from "../user/service/user.service";
import {JwtModule} from "@nestjs/jwt";
import {PrismaService} from "../prisma.service";
import {PassportModule} from "@nestjs/passport";
import {AuthController} from './controller/auth.controller';
import {JwtStrategy} from "./jwt-strategy";
@Module({
    imports: [
        PassportModule.register({defaultStrategy: 'jwt'}),
        JwtModule.register({}),
    ],
    providers: [AuthService, UserService, PrismaService, JwtStrategy],
    controllers: [AuthController]
})
export class AuthModule {
}
