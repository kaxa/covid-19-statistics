export type SignInResponse = {
    accessToken: string
}

export type SignUpResponse = {
    id: number,
    name: string,
    email: string,
    updatedAt: Date,
    createdAt: Date,
}
