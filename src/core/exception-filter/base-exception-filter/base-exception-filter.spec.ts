import {BaseExceptionsFilter} from "./base-exception-filter";
import {BaseExceptionFilterError} from "./type/base-exception-filter.interface";

describe('BaseExceptionsFilter', () => {

    let baseExceptionsFilter: BaseExceptionsFilter<any>;

    beforeEach(async () => {
        baseExceptionsFilter = new BaseExceptionsFilter<any>();
    });

    it('should be defined', () => {
        expect(baseExceptionsFilter).toBeDefined();
    });

    describe('getErrorLog', () => {

        it('should return specific result', async () => {

            baseExceptionsFilter['writeErrorLogToFile'] = jest.fn().mockReturnValueOnce({});

            const mockErrorResponse: BaseExceptionFilterError = {
                error: "error",
                method: "test",
                path: "test",
                message: "error message",
                timeStamp: new Date('2022-05-02T13:31:10.691Z'),
                statusCode: 200
            };

            const mockRequest: any = {
                method: "testMethod",
                url: "testUrl"
            };

            const mockException = new Error('testError');

            const result = baseExceptionsFilter['getErrorLog'](
                mockErrorResponse, mockRequest, mockException
            )
            const expectedResult =
                "Response Code: 200 - Method: testMethod - URL: testUrl\n" +
                "\n" +
                "\n" +
                "    {\"error\":\"error\",\"method\":\"test\",\"path\":\"test\",\"message\":\"error message\",\"timeStamp\":\"2022-05-02T13:31:10.691Z\",\"statusCode\":200}\n" +
                "\n" +
                "\n" +
                "    error"
                + "\n" +
                "\n"

            expect(result).toBe(expectedResult);

        });

    });

});
