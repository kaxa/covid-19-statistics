export interface HttpExceptionResponse {
  statusCode: number;
  error: string;
  message:string
}

export interface BaseExceptionFilterError extends HttpExceptionResponse {
  path: string;
  method: string;
  timeStamp: Date;
}
