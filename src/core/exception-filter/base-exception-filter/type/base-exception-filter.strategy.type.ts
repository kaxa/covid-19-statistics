import { BaseExceptionFilterError } from './base-exception-filter.interface';
import { Request } from 'express';

export interface IBaseExceptionFilterFactory {
  buildError()
}

export abstract class BaseExceptionFilterFactoryAbstract implements IBaseExceptionFilterFactory {

  protected exception: any;
  protected request: Request;

  /**
   * @param  {any} exception
   * @param  {Request} request
   */
  protected constructor(exception: any, request: Request) {
    this.exception = exception;
    this.request = request;
  }

  /**
   * builds error by exception
   * @return {BaseExceptionFilterError}  exception filter error object
   */
  public abstract buildError(): BaseExceptionFilterError

}
