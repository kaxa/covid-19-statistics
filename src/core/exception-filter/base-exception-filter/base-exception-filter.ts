import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { BaseExceptionFilterError } from './type/base-exception-filter.interface';
import * as fs from 'fs';

import { IBaseExceptionFilterFactory } from './type/base-exception-filter.strategy.type';
import { BaseExceptionFilterErrorCreator } from './factory/creator/base-exception-filter-error.creator';

@Catch()
export class BaseExceptionsFilter<T> implements ExceptionFilter {

  /**
   * builds error response object
   * @param  {any} exception
   * @param  {ArgumentsHost} host
   */
  catch(exception: T, host: ArgumentsHost) {

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    const baseExceptionFilterFactoryCreator: BaseExceptionFilterErrorCreator = new BaseExceptionFilterErrorCreator();

    const baseExceptionFilterStrategyAbstract: IBaseExceptionFilterFactory = baseExceptionFilterFactoryCreator.createObject(exception, request);

    const errorResponse: BaseExceptionFilterError = baseExceptionFilterStrategyAbstract.buildError();

    // write logs
    const errorLog = this.getErrorLog(errorResponse, request, exception);
    this.writeErrorLogToFile(errorLog);

    response.status(errorResponse.statusCode).json(errorResponse);

  }

  /**
   * build error log string
   * @param  {BaseExceptionFilterError} errorResponse error response object
   * @param  {Request} request
   * @param  {} exception
   * @return {string}
   */
  private getErrorLog = (errorResponse: BaseExceptionFilterError, request: Request, exception: T): string => {

    const { statusCode, error } = errorResponse;
    const { method, url } = request;
    return `Response Code: ${statusCode} - Method: ${method} - URL: ${url}\n\n
    ${JSON.stringify(errorResponse)}\n\n
    ${exception instanceof HttpException ? exception.stack : error}\n\n`;

  };

  /**
   * write error logs to file
   * @param  {string} errorLog error log string object
   */
  private writeErrorLogToFile = (errorLog: string): void => {
    fs.appendFile('error.log', errorLog, 'utf8', (err) => {
      if (err) throw err;
    });
  };

}
