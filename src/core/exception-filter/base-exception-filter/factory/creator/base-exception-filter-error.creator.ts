import {
    IBaseExceptionFilterFactory,
} from '../../type/base-exception-filter.strategy.type';
import {HttpException} from '@nestjs/common';
import {HttpBaseExceptionFilterError} from '../concrete/http-base-exception-filter-error/http-base-exception-filter-error';
import {Request} from 'express';
import {Prisma} from '@prisma/client';
import {PrismaKnownBaseExceptionFilterError} from '../concrete/prisma-known-base-exception-filter-error/prisma-known-base-exception-filter-error';
import {UnknownBaseExceptionFilterError} from '../concrete/unknown-base-exception-filter-error/unknown-base-exception-filter-error';

export class BaseExceptionFilterErrorCreator {

    /**
     * creates base exception filter error class
     * depends on exception type
     * @param  {any} exception
     * @param  {Request} request
     * @return {IBaseExceptionFilterFactory}  IBaseExceptionFilterFactory type class
     */
    public createObject(exception: any, request: Request): IBaseExceptionFilterFactory {

        if (exception instanceof HttpException) {

            return new HttpBaseExceptionFilterError(exception, request);

        } else if (exception instanceof Prisma.PrismaClientKnownRequestError) {

            if (exception.code === 'P2002') {
                return new PrismaKnownBaseExceptionFilterError(exception, request);
            }

        }

        return new UnknownBaseExceptionFilterError(exception, request);


    }

}
