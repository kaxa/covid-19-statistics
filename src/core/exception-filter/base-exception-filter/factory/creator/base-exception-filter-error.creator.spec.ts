import {BaseExceptionFilterErrorCreator} from "./base-exception-filter-error.creator";
import {BadRequestException} from "@nestjs/common";
import {
    HttpBaseExceptionFilterError
} from "../concrete/http-base-exception-filter-error/http-base-exception-filter-error";
import {Prisma} from "@prisma/client";
import {
    PrismaKnownBaseExceptionFilterError
} from "../concrete/prisma-known-base-exception-filter-error/prisma-known-base-exception-filter-error";
import {
    UnknownBaseExceptionFilterError
} from "../concrete/unknown-base-exception-filter-error/unknown-base-exception-filter-error";

describe('HttpBaseExceptionFilterError', () => {

    let baseExceptionFilterErrorCreator: BaseExceptionFilterErrorCreator;

    beforeEach(async () => {

        baseExceptionFilterErrorCreator = new BaseExceptionFilterErrorCreator();

    });

    describe('createObject', () => {

        it('should return object type HttpBaseExceptionFilterError', async () => {

            const mockRequest: any = {
                url: "testUrl",
                method: "post"
            };

            const result = baseExceptionFilterErrorCreator.createObject(new BadRequestException(), mockRequest);

            expect(result instanceof HttpBaseExceptionFilterError).toBe(true);

        });

        it('should return PrismaKnownBaseExceptionFilterError when error code is P2002', async () => {

            const mockRequest: any = {
                url: "testUrl",
                method: "post"
            };

            const result = baseExceptionFilterErrorCreator.createObject(new Prisma.PrismaClientKnownRequestError("test", 'P2002', ''), mockRequest);
            expect(result instanceof PrismaKnownBaseExceptionFilterError).toBe(true);

        });

        it('should return UnknownBaseExceptionFilterError when error code is not P2002', async () => {

            const mockRequest: any = {
                url: "testUrl",
                method: "post"
            };

            const result = baseExceptionFilterErrorCreator.createObject(new Prisma.PrismaClientKnownRequestError("test", 'P2001', ''), mockRequest);
            expect(result instanceof UnknownBaseExceptionFilterError).toBe(true);

        });

        it('should return UnknownBaseExceptionFilterError on any unknown error', async () => {

            const mockRequest: any = {
                url: "testUrl",
                method: "post"
            };

            const result = baseExceptionFilterErrorCreator.createObject(new TypeError(), mockRequest);

            expect(result instanceof UnknownBaseExceptionFilterError).toBe(true);

        });

    });

});
