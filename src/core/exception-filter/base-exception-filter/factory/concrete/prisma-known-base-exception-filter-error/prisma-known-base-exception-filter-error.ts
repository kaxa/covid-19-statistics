import { BaseExceptionFilterError } from '../../../type/base-exception-filter.interface';
import {
  BaseExceptionFilterFactoryAbstract,
} from '../../../type/base-exception-filter.strategy.type';
import { HttpStatus } from '@nestjs/common';
import { Request } from 'express';
import { Prisma } from '@prisma/client';

export class PrismaKnownBaseExceptionFilterError extends BaseExceptionFilterFactoryAbstract {

  constructor(exception: Prisma.PrismaClientKnownRequestError, request: Request) {
    super(exception, request);
  }

  /**
   * builds error by exception
   * @return {BaseExceptionFilterError}  exception filter error object
   */
  buildError(): BaseExceptionFilterError {

    const status = HttpStatus.CONFLICT;
    const errorMessage = 'Already exist';
    const errorName = 'Conflict';

    return {
      statusCode: status,
      error: errorName,
      path: this.request.url,
      method: this.request.method,
      timeStamp: new Date(),
      message: errorMessage,
    };
  }

}
