import {BaseExceptionFilterError} from "../../../type/base-exception-filter.interface";
import {PrismaKnownBaseExceptionFilterError} from "./prisma-known-base-exception-filter-error";
import {Prisma} from "@prisma/client";

describe('PrismaKnownBaseExceptionFilterError', () => {

    let prismaKnownBaseExceptionFilterError: PrismaKnownBaseExceptionFilterError;

    beforeEach(async () => {
        const exception = new Prisma.PrismaClientKnownRequestError("test", 'p002', '');
        const mockRequest: any = {
            url: "testUrl",
            method: "post"
        };

        prismaKnownBaseExceptionFilterError = new PrismaKnownBaseExceptionFilterError(exception, mockRequest);

    });

    describe('buildError', () => {

        it('should return specific BaseExceptionFilterError when Prisma already known error', async () => {

            const currDate = new Date();

            const expectedResult: BaseExceptionFilterError = {
                error: 'Conflict',
                path: 'testUrl',
                method: 'post',
                timeStamp: currDate,
                message: 'Already exist',
                statusCode: 409
            };

            const result = prismaKnownBaseExceptionFilterError.buildError();
            result.timeStamp = currDate;
            expect(result).toMatchObject(expectedResult);

        });

    });

});
