import {BaseExceptionFilterError} from "../../../type/base-exception-filter.interface";
import {UnknownBaseExceptionFilterError} from "./unknown-base-exception-filter-error";
import {CircularDependencyException} from "@nestjs/core/errors/exceptions/circular-dependency.exception";

describe('HttpBaseExceptionFilterError', () => {

    let unknownBaseExceptionFilterError: UnknownBaseExceptionFilterError;

    beforeEach(async () => {

        const exception = new CircularDependencyException();

        const mockRequest: any = {
            url: "testUrl",
            method: "post"
        };

        unknownBaseExceptionFilterError = new UnknownBaseExceptionFilterError(exception, mockRequest);

    });

    describe('buildError', () => {

        it('should return specific BaseExceptionFilterError when unknown exception', async () => {

            const currDate = new Date();

            const expectedResult: BaseExceptionFilterError = {
                error: 'Internal server error',
                path: 'testUrl',
                method: 'post',
                timeStamp: currDate,
                message: 'Unknown error occurred!',
                statusCode: 500
            };

            const result = unknownBaseExceptionFilterError.buildError();
            result.timeStamp = currDate;
            expect(result).toMatchObject(expectedResult);

        });

    });

});
