import { BaseExceptionFilterError } from '../../../type/base-exception-filter.interface';
import {
  BaseExceptionFilterFactoryAbstract,
} from '../../../type/base-exception-filter.strategy.type';
import { HttpStatus } from '@nestjs/common';
import { Request } from 'express';

export class UnknownBaseExceptionFilterError extends BaseExceptionFilterFactoryAbstract {

  constructor(exception: any, request: Request) {
    super(exception, request);
  }

  /**
   * builds error by exception
   * @return {BaseExceptionFilterError}  exception filter error object
   */
  buildError(): BaseExceptionFilterError {

    const status = HttpStatus.INTERNAL_SERVER_ERROR;
    const errorMessage = 'Unknown error occurred!';
    const errorName = 'Internal server error';

    return {
      statusCode: status,
      error: errorName,
      path: this.request.url,
      method: this.request.method,
      timeStamp: new Date(),
      message: errorMessage,
    };
  }

}
