import {
  BaseExceptionFilterFactoryAbstract,
} from '../../../type/base-exception-filter.strategy.type';
import { BaseExceptionFilterError, HttpExceptionResponse } from '../../../type/base-exception-filter.interface';
import { HttpException } from '@nestjs/common';
import { Request } from 'express';

export class HttpBaseExceptionFilterError extends BaseExceptionFilterFactoryAbstract {

  constructor(exception: HttpException, request: Request) {
    super(exception, request);
  }

  /**
   * builds error by exception
   * @return {BaseExceptionFilterError}  exception filter error object
   */
  buildError(): BaseExceptionFilterError {

    const status = this.exception.getStatus();
    const errorResponse = this.exception.getResponse();

    const errorName = (errorResponse as HttpExceptionResponse).error || this.exception.message;
    const errorMessage = (errorResponse as HttpExceptionResponse).message || '';

    return {
      statusCode: status,
      error: errorName,
      path: this.request.url,
      method: this.request.method,
      timeStamp: new Date(),
      message: errorMessage,
    };
  }

}
