import {HttpBaseExceptionFilterError} from "./http-base-exception-filter-error";
import {BadRequestException} from "@nestjs/common";
import {BaseExceptionFilterError} from "../../../type/base-exception-filter.interface";

describe('HttpBaseExceptionFilterError', () => {

    let httpBaseExceptionFilterError: HttpBaseExceptionFilterError;

    beforeEach(async () => {
        const exception = new BadRequestException();
        const mockRequest: any = {
            url: "testUrl",
            method: "post"
        };

        httpBaseExceptionFilterError = new HttpBaseExceptionFilterError(exception, mockRequest);

    });

    describe('buildError', () => {

        it('should return specific BaseExceptionFilterError when Bad request', async () => {

            const currDate = new Date();

            const expectedResult: BaseExceptionFilterError = {
                error: 'Bad Request',
                path: 'testUrl',
                method: 'post',
                timeStamp: currDate,
                message: 'Bad Request',
                statusCode: 400
            };

            const result = httpBaseExceptionFilterError.buildError();
            result.timeStamp = currDate;
            expect(result).toMatchObject(expectedResult);

        });

    });

});
