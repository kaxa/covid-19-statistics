FROM node as development

WORKDIR /usr/src/app

COPY package*.json ./
COPY prisma ./prisma/

RUN npm add glob rimraf

RUN npm install

COPY . .

RUN npm run build
